/**
 *
 * H3news
 *
 */

import React, { memo } from 'react';
import { Row, Image } from 'antd';
import PropTypes from 'prop-types';
import TitleCom from '../TitleCom/Loadable';
// import styled from 'styled-components';

function H3news(props) {
  const row = [];
  for (let i = 0; i < props.num; i += 1) {
    row.push(
      <Row style={{ textAlign: 'center' }}>
        <Image width={props.mWidth} src={props.mSrc} />
        <a href={props.mLink}>{props.mTitle}</a>
        <p>{props.mDay}</p>
      </Row>,
    );
  }
  return <TitleCom mCategory={props.mCategory} cont={row} />;
}

H3news.propTypes = {
  num: PropTypes.number,
  mWidth: PropTypes.string,
  mSrc: PropTypes.string,
  mLink: PropTypes.string,
  mTitle: PropTypes.string,
  mDay: PropTypes.string,
  mCategory: PropTypes.string,
};

export default memo(H3news);
