/**
 *
 * Asynchronously loads the component for H3news
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
