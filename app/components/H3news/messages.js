/*
 * H3news Messages
 *
 * This contains all the text for the H3news component.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.components.H3news';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the H3news component!',
  },
});
