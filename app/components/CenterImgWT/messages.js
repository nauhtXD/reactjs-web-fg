/*
 * CenterImgWt Messages
 *
 * This contains all the text for the CenterImgWt component.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.components.CenterImgWt';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the CenterImgWt component!',
  },
});
