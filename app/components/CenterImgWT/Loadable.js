/**
 *
 * Asynchronously loads the component for CenterImgWt
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
