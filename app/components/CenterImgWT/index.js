/**
 *
 * CenterImgWt
 *
 */

import React, { memo } from 'react';
import { Image } from 'antd';
import PropTypes from 'prop-types';

function CenterImgWT(props) {
  return (
    <div>
      <Image
        width={props.mWidth}
        src={props.mSrc}
        style={{ display: 'block', margin: '2px auto', width: '50%' }}
      />
      <a href={props.mLink}>{props.mTitle}</a>
    </div>
  );
}

CenterImgWT.propTypes = {
  mWidth: PropTypes.string,
  mSrc: PropTypes.string,
  mLink: PropTypes.string,
  mTitle: PropTypes.string,
};

export default memo(CenterImgWT);
