/**
 *
 * Boxws
 *
 */

import React, { memo } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const Bws = styled.div`
  background-color: #fff;
  border: 1px solid;
  padding: 10px;
  box-shadow: 2px 2px silver;
  border-radius: 10px;
  margin: 10px 10px 0 10px;
  height: auto;
`;

function Boxws(props) {
  return <Bws>{props.cont}</Bws>;
}

Boxws.propTypes = {
  cont: PropTypes.any,
};

export default memo(Boxws);
