/*
 * Boxws Messages
 *
 * This contains all the text for the Boxws component.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.components.Boxws';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the Boxws component!',
  },
});
