/**
 *
 * Asynchronously loads the component for Boxws
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
