/**
 *
 * Header
 *
 */

import React from 'react';
import { Menu } from 'antd';
import { UserOutlined } from '@ant-design/icons';
// import PropTypes from 'prop-types';
import styled from 'styled-components';

const { SubMenu } = Menu;

const MMenu = styled(Menu)`
  position: fixed;
  z-index: 1;
  width: 90%;
`;

const MLink = styled.a`
  color: #fff;
  ${MLink}:hover & {
    color: red;
  }
`;

function Header() {
  return (
    <div>
      <MMenu mode="horizontal" style={{ backgroundColor: '#009000' }}>
        <Menu.Item>
          <MLink href="/">TRANG CHỦ</MLink>
        </Menu.Item>
        <Menu.Item>
          <a href=".">GIỚI THIỆU</a>
        </Menu.Item>
        <SubMenu title="TIN TỨC - SỰ KIỆN">
          <Menu.Item key="encourage">KHUYẾN NÔNG</Menu.Item>
          <Menu.Item key="farming">TRỒNG TRỌT</Menu.Item>
          <Menu.Item key="ranching">CHĂN NUÔI</Menu.Item>
        </SubMenu>
        <Menu.Item>
          <a href=".">HOẠT ĐỘNG CỦA HỘI</a>
        </Menu.Item>
        <Menu.Item>
          <a href=".">VĂN BẢN HỘI</a>
        </Menu.Item>
        <Menu.Item>
          <a href=".">NHÀ NÔNG CẦN BIẾT</a>
        </Menu.Item>
        <Menu.Item>
          <a href=".">LIÊN HỆ</a>
        </Menu.Item>
        <Menu.Item
          key="login"
          style={{ float: 'right' }}
          icon={<UserOutlined />}
        >
          Đăng nhập
        </Menu.Item>
      </MMenu>
    </div>
  );
}

Header.propTypes = {};

export default Header;
