/**
 *
 * Asynchronously loads the component for MFieldset
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
