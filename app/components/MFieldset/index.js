/**
 *
 * MFieldset
 *
 */

import React, { memo } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const MFs = styled.fieldset`
  border: 1px solid silver;
  border-radius: 4px;
  margin: 8px;
  padding: 8px;
`;

const MLg = styled.legend`
  padding: 2px;
  width: auto;
`;

function MFieldset(props) {
  return (
    <MFs>
      <MLg>{props.mCategory}</MLg>
      {props.cont}
    </MFs>
  );
}

MFieldset.propTypes = {
  mCategory: PropTypes.string,
  cont: PropTypes.any,
};

export default memo(MFieldset);
