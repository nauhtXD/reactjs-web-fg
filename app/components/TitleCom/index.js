/**
 *
 * TitleCom
 *
 */

import React, { memo } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const OSDiv = styled.div`
  border: 1px solid silver;
  margin: 0 10px 10px 0;
`;

const HDiv = styled.div`
  border-bottom: 1px solid #22b14c;
  margin-bottom: 5px;
  color: #22b14c;
  background-color: #deffc0;
  font-size: 20px;
`;

function TitleCom(props) {
  return (
    <OSDiv>
      <HDiv>{props.mCategory}</HDiv>
      {props.cont}
      <div style={{ textAlign: 'right' }}>
        <a href=".">Xem thêm</a>
      </div>
    </OSDiv>
  );
}

TitleCom.propTypes = {
  mCategory: PropTypes.string,
  cont: PropTypes.any,
};

export default memo(TitleCom);
