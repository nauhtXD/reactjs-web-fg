/**
 *
 * App
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 */

import React from 'react';
import { Helmet } from 'react-helmet';
// import styled from 'styled-components';
import { Switch, Route } from 'react-router-dom';

import NotFoundPage from 'containers/NotFoundPage/Loadable';
import Home from '../Home/Loadable';
import News from '../News/Loadable';
import Admin from '../Admin/Loadable';
import Post from '../Admin/pages/post';
import User from '../Admin/pages/user';
import 'antd/dist/antd.css';

export default function App() {
  return (
    <div>
      <Helmet
        titleTemplate="%s - Hội quán nông dân"
        defaultTitle="Hội quán nông dân"
      >
        <meta name="description" content="Hội quán nông dân" />
      </Helmet>
      <Switch>
        <Route exact path="/" component={Home} />
        <Route exact path="/News" component={News} />
        <Route path="/Admin" component={Admin} />
        <Route exact path="/Admin/Post" component={Post} />
        <Route exact path="/Admin/User" component={User} />
        <Route path="" component={NotFoundPage} />
      </Switch>
    </div>
  );
}
