/* eslint-disable jsx-a11y/no-distracting-elements */
/* eslint-disable no-plusplus */
import React, { memo } from 'react';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import styled from 'styled-components';
import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import { Row, Col, Input, Carousel, Layout } from 'antd';
import reducer from './reducer';
import saga from './saga';
import Header from '../../components/Header/Loadable';
import Footer from '../../components/Footer/Loadable';
import H3news from '../../components/H3news/Loadable';
import CenterImgWT from '../../components/CenterImgWT/Loadable';
import TitleCom from '../../components/TitleCom/Loadable';

const { Search } = Input;
const { Content, Sider } = Layout;

// #region  row
const row = [];
for (let i = 0; i < 4; i++) {
  row.push(
    <Col span={6} style={{ textAlign: 'center' }}>
      <CenterImgWT
        mWidth="150px"
        mSrc="https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png"
        mLink="/News"
        mTitle="Title2"
      />
    </Col>,
  );
}

const rowc = [];
for (let i = 0; i < 3; i++) {
  rowc.push(
    <Col span={8} style={{ textAlign: 'center' }}>
      <CenterImgWT
        mWidth="150px"
        mSrc="https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png"
        mLink="/News"
        mTitle="Title2"
      />
    </Col>,
  );
}

const rowh = [];
for (let i = 0; i < 3; i++) {
  rowh.push(
    <Col span={8}>
      <H3news
        num="3"
        mWidth="150px"
        mSrc="https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png"
        mLink="/News"
        mTitle="Title2"
        mDay="dd/MM/yyyy"
        mCategory="Category"
      />
    </Col>,
  );
}
// #endregion

// #region  list data
const data = [];
for (let i = 0; i <= 10; i++) {
  data.push({
    href: '',
    title: `test part ${i}`,
  });
}
// #endregion

// #region styled
const ClearDiv = styled.div`
  clear: both;
  height: 2px;
`;
const ContentStyle = styled.h3`
  height: 160px;
  color: #fff;
  lineheight: 160px;
  textalign: center;
  background: #364d79;
`;
// #endregion

export function Home() {
  useInjectReducer({ key: 'home', reducer });
  useInjectSaga({ key: 'home', saga });

  return (
    <div>
      <Helmet>
        <title>Home</title>
        <meta name="description" content="Description of Home" />
      </Helmet>
      <div style={{ width: '90%', margin: 'auto' }}>
        <Header />
        <div>
          <Carousel autoplay effect="fade">
            <div>
              <ContentStyle>1</ContentStyle>
            </div>
            <div>
              <ContentStyle>2</ContentStyle>
            </div>
            <div>
              <ContentStyle>3</ContentStyle>
            </div>
            <div>
              <ContentStyle>4</ContentStyle>
            </div>
          </Carousel>
          <ClearDiv />
          <Row>
            <Col span={20} style={{ textAlign: 'left' }}>
              <p style={{ fontSize: '20px' }}>12/12/2020</p>
            </Col>
            <Col span={4}>
              <Search placeholder="input search text" />
            </Col>
          </Row>
        </div>
        <ClearDiv />
        <Layout>
          <Content style={{ backgroundColor: '#fff' }}>
            <TitleCom
              mCategory="Tin tức - Sự kiện"
              cont={
                <div>
                  <Row>{row}</Row>
                  <Row>{row}</Row>
                </div>
              }
            />
            <TitleCom
              mCategory="Nhà nông cần biết"
              cont={
                <div>
                  <marquee width="100%" behavior="scroll">
                    <Row>{rowc}</Row>
                  </marquee>
                </div>
              }
            />
          </Content>
          <Sider
            style={{ backgroundColor: '#fff', border: '1px solid silver' }}
          />
        </Layout>
        <Row>{rowh}</Row>
        <ClearDiv />
      </div>
      <Footer />
    </div>
  );
}

Home.propTypes = {};

const mapStateToProps = createStructuredSelector({});

const mapDispatchToProps = () => ({});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(Home);
