import React, { memo } from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { Breadcrumb, Rate, Typography, List } from 'antd';
import { HomeOutlined } from '@ant-design/icons';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import reducer from './reducer';
import saga from './saga';
import Header from '../../components/Header/Loadable';
import Footer from '../../components/Footer/Loadable';

const data = [
  'Racing car sprays burning fuel into crowd.',
  'Japanese princess to wed commoner.',
  'Australian walks 100km after outback crash.',
  'Man charged over missing wedding girl.',
  'Los Angeles battles huge wildfires.',
];

export function News() {
  useInjectReducer({ key: 'news', reducer });
  useInjectSaga({ key: 'news', saga });

  return (
    <div>
      <Header />
      <Breadcrumb>
        <Breadcrumb.Item href="">
          <HomeOutlined />
        </Breadcrumb.Item>
        <Breadcrumb.Item href="">
          <span>Application List</span>
        </Breadcrumb.Item>
        <Breadcrumb.Item>Application</Breadcrumb.Item>
      </Breadcrumb>
      <p style={{ textAlign: 'left' }}>Tiêu đề</p>
      <p style={{ textAlign: 'right' }}>Ngày</p>
      <div />
      <p style={{ textAlign: 'right' }}>Nguồn: </p>
      <div />
      <div>
        <Rate />
        <div />
      </div>
      <div>
        <h2>Tin liên quan</h2>
        <div>
          <List
            bordered
            dataSource={data}
            renderItem={item => (
              <List.Item>
                <Typography.Text mark>[dd/MM/yyyy]</Typography.Text>
                {item}
              </List.Item>
            )}
          />
        </div>
      </div>
      <Footer />
    </div>
  );
}

News.propTypes = {};

const mapStateToProps = createStructuredSelector({});

const mapDispatchToProps = () => ({});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(News);
