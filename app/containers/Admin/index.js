import React, { memo } from 'react';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
// import styled from 'styled-component';
import { Menu, Layout, Divider } from 'antd';
import { LogoutOutlined } from '@ant-design/icons';
import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import { Route, Link } from 'react-router-dom';
import reducer from './reducer';
import saga from './saga';
import User from './pages/user';
import Post from './pages/post';

const { SubMenu } = Menu;
const { Sider, Content } = Layout;

export function Admin() {
  useInjectReducer({ key: 'admin', reducer });
  useInjectSaga({ key: 'admin', saga });

  return (
    <div>
      <Helmet>
        <title>Admin</title>
        <meta name="description" content="Description of Admin" />
      </Helmet>
      <Layout>
        <Sider style={{ backgroundColor: '#fff' }}>
          <Menu
            defaultSelectedKeys={['1']}
            defaultOpenKeys={['sub1']}
            mode="inline"
          >
            <SubMenu key="sub1" title="Hộ dân">
              <Menu.Item key="1">
                <Link to="/Admin/User">Tài khoản</Link>
              </Menu.Item>
              <Menu.Item key="2">
                <Link to="/Admin/Households">Thông tin hộ</Link>
              </Menu.Item>
            </SubMenu>
            <Divider />
            <Menu.Item key="logout" icon={<LogoutOutlined />}>
              <a href="/">Đăng xuất</a>
            </Menu.Item>
          </Menu>
        </Sider>
        <Content>
          <Route path="/Admin/User" component={User} />
          <Route path="/Admin/Post" component={Post} />
        </Content>
      </Layout>
    </div>
  );
}

Admin.propTypes = {};

const mapStateToProps = createStructuredSelector({});

const mapDispatchToProps = () => ({});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(Admin);
