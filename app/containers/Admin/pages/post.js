import React, { memo } from 'react';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { Row, Col, DatePicker, Select, Input, Button, Image } from 'antd';
// import styled from 'styled-components';
import moment from 'moment';
import ReactQuill from 'react-quill';
import Boxws from '../../../components/Boxws/Loadable';
import 'react-quill/dist/quill.snow.css';

const dateFormat = 'DD/MM/YYYY';
const { Option } = Select;

export function Post() {
  return (
    <div>
      <Helmet>
        <title>Posts</title>
        <meta name="description" content="Description of posts" />
      </Helmet>
      <div>
        <p
          style={{
            fontSize: '30px',
            margin: '10px 10px 0 10px',
            textAlign: 'center',
          }}
        >
          Post
        </p>
        <Row>
          <Col span={16}>
            <Boxws
              cont={
                <Row>
                  <Col span={2}>
                    <h4 style={{ margin: '5px' }}>Title</h4>
                  </Col>
                  <Col span={22}>
                    <Input placeholder="Title" />
                  </Col>
                </Row>
              }
            />
            <Boxws
              cont={
                <ReactQuill
                  theme="snow"
                  modules={Post.modules}
                  formats={Post.formats}
                  bounds=".app"
                  style={{ height: '450px' }}
                />
              }
            />
          </Col>
          <Col span={8}>
            <Boxws
              cont={
                <Image
                  width={200}
                  src="https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png"
                />
              }
            />
            <Boxws
              cont={
                <div>
                  <Row>
                    <Col span={6}>
                      <h4>Danh mục:</h4>
                      <h4>Nguồn:</h4>
                      <h4>Ngày viết:</h4>
                    </Col>
                    <Col span={18}>
                      <Select defaultValue="lucy" style={{ width: '100%' }}>
                        <Option value="lucy">Lucy</Option>
                        <Option value="jack">Jack</Option>
                        <Option value="windy">Windy</Option>
                      </Select>
                      <Input placeholder="Source" />
                      <DatePicker
                        defaultValue={moment(moment().date, dateFormat)}
                        format={dateFormat}
                        style={{ width: '100%' }}
                      />
                    </Col>
                  </Row>
                  <Button type="primary">Đăng</Button>
                </div>
              }
            />
          </Col>
        </Row>
      </div>
    </div>
  );
}

Post.modules = {
  toolbar: [
    [{ header: [1, 2, false] }],
    ['bold', 'italic', 'underline', 'strike', 'blockquote'],
    [
      { list: 'ordered' },
      { list: 'bullet' },
      { indent: '-1' },
      { indent: '+1' },
    ],
    ['link', 'image'],
    ['clean'],
  ],
};

Post.formats = [
  'header',
  'bold',
  'italic',
  'underline',
  'strike',
  'blockquote',
  'list',
  'bullet',
  'indent',
  'link',
  'image',
];

Post.propTypes = {};

const mapStateToProps = createStructuredSelector({});

const mapDispatchToProps = () => ({});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(Post);
