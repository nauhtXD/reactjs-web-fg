import React, { memo, useState } from 'react';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { Modal, Button, Table, Row, Col, Select, Input } from 'antd';
import styled from 'styled-components';

const columns = [
  {
    title: 'Name',
    dataIndex: 'name',
  },
  {
    title: 'Chinese Score',
    dataIndex: 'chinese',
    sorter: {
      compare: (a, b) => a.chinese - b.chinese,
      multiple: 3,
    },
  },
  {
    title: 'Math Score',
    dataIndex: 'math',
    sorter: {
      compare: (a, b) => a.math - b.math,
      multiple: 2,
    },
  },
  {
    title: 'English Score',
    dataIndex: 'english',
    sorter: {
      compare: (a, b) => a.english - b.english,
      multiple: 1,
    },
  },
];

const data = [
  {
    key: '1',
    name: 'John Brown',
    chinese: 98,
    math: 60,
    english: 70,
  },
  {
    key: '2',
    name: 'Jim Green',
    chinese: 98,
    math: 66,
    english: 89,
  },
  {
    key: '3',
    name: 'Joe Black',
    chinese: 98,
    math: 90,
    english: 70,
  },
  {
    key: '4',
    name: 'Jim Red',
    chinese: 88,
    math: 99,
    english: 89,
  },
];

const Bws = styled.div`
  background-color: #fff;
  border: 1px solid;
  padding: 10px;
  box-shadow: 2px 2px silver;
  border-radius: 10px;
  margin: 10px 10px 0 10px;
  height: auto;
`;

const { Option } = Select;
const { Search } = Input;

export function User() {
  const [isVisible, setIsVisible] = useState(false);
  return (
    <div>
      <Helmet>
        <title>User</title>
        <meta name="description" content="Description of User" />
      </Helmet>
      <div>
        <div style={{ textAlign: 'center' }}>
          <p style={{ fontSize: '30px', margin: '10px 0 10px 0' }}>
            List of users
          </p>
          <Bws>
            <Row>
              <Col span={4}>
                <Select defaultValue="jack" style={{ width: 120 }}>
                  <Option value="jack">Jack</Option>
                  <Option value="lucy">Lucy</Option>
                  <Option value="Yiminghe">yiminghe</Option>
                </Select>
              </Col>
              <Col span={16}>
                <Search placeholder="input text here" />
              </Col>
              <Col span={4}>
                <Button type="primary" onClick={() => setIsVisible(true)}>
                  Create new
                </Button>
              </Col>
            </Row>
          </Bws>
        </div>
        <div style={{ clear: 'both', height: '10px' }} />
        <Bws>
          <Table columns={columns} dataSource={data} />
        </Bws>
      </div>
      <Modal
        title="Create new user"
        centered
        visible={isVisible}
        onOk={() => setIsVisible(false)}
        onCancel={() => setIsVisible(false)}
        footer={[
          <Button key="back" onClick={() => setIsVisible(false)}>
            Return
          </Button>,
          <Button
            key="submit"
            type="primary"
            onClick={() => setIsVisible(false)}
          >
            Submit
          </Button>,
        ]}
      >
        <Row>
          <Col span={6}>
            <h4 style={{ margin: '10px' }}>ID: </h4>
          </Col>
          <Col span={18}>
            <Input placeholder="User ID" style={{ margin: '5px' }} />
          </Col>
        </Row>
        <Row>
          <Col span={6}>
            <h4 style={{ margin: '10px' }}>Password: </h4>
          </Col>
          <Col span={18}>
            <Input
              placeholder="Password"
              type="password"
              style={{ margin: '5px 0' }}
            />
          </Col>
        </Row>
        <p>Nhập lại mật khẩu: </p>
        <p>Số điện thoại: </p>
        <p>Email: </p>
        <p>Loại tài khoản: </p>
      </Modal>
    </div>
  );
}

User.propTypes = {};

const mapStateToProps = createStructuredSelector({});

const mapDispatchToProps = () => ({});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(User);
